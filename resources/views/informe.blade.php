<x-layout bodyClass="g-sidenav-show  bg-gray-200">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
<script src="{{ asset('/vendor/ckeditor/ckeditor.js') }}"></script>
        <x-navbars.sidebar activePage="Informe"></x-navbars.sidebar>
        <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
            <!-- Navbar -->
            <x-navbars.navs.auth titlePage="Informe"></x-navbars.navs.auth>
            <!-- End Navbar -->
            <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card my-4">
                            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                    <h6 class="text-white text-capitalize ps-3">Formulario de generacion de informe</h6>
                                    <!--<p class="text-white text-capitalize  ps-3">Oficinas</p>-->                            
                                </div>
                            </div>
                            <div class="card-body px-0 pb-2">
                                <!--card de creacion de oficinas-->
                                @php($creador = $nombre_completo->name.' '.$nombre_completo->apellido_paterno.' '.$nombre_completo->apellido_materno)
                                <form action="{{ route('guardar_informe') }}" method="post" enctype="multipart/form-data">
                                     @csrf
                                    <div class="card">
                                        <div class="card-header card-header-info">
                                                    <h1 class="card-title text-center">DATOS CABECERA</h1>
                                                    <h4 class="card-title text-center"><span class="text-danger">(*)</span>Campos Obligatorios</h4>
                                        </div>
                                        <div class="card-body">
                                                <div class="row">
                                                    <input type="hidden" value="{{$nombre_completo->id}}" name="id_usuario_generador" id="id_usuario_generador">
                                                    <input type="hidden" value="{{$creador}}" name="usuario[]" id="usuario">
                                                    <input type="hidden" value="{{$nombre_completo->cargo}}" name="cargo[]" id="cargo">
                                                    <input type="hidden" value="{{$nombre_completo->unidad}}" name="unidad[]" id="unidad">
                                                    <input type="hidden" value="{{$nombre_completo->firma}}" name="firma[]" id="firma">

                                                    <input type="hidden" value="Pendiente" name="estado" id="estado">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                                <div class="input-group input-group-static is-valid mb-4">
                                                                    <label for="dirigido_a">Dirigido a: <span class="text-danger">(*)</span></label>
                                                                    <input type="text" list="nombre_dirigido" name="nombre_dirigido" id="nombre_dirigidos" class="form-control"  value="{{old('nombre_dirigido')}}">
                                                                    <datalist id="nombre_dirigido">
                                                                    @foreach($nombres_funcionarios->data as $nombres_funcionario)
                                                                    @php($nombre_funcionario = $nombres_funcionario->nombres.' '.$nombres_funcionario->ap_paterno.' '.$nombres_funcionario->ap_materno )
                                                                        <option data-unidad="{{$nombres_funcionario->unidad}}" data-cargo="{{$nombres_funcionario->cargo}}" value="{{$nombre_funcionario}}">
                                                                    @endforeach
                                                                    </datalist>

                                                                </div>                                                       
                                                        </div>
                                                        @error('nombre_dirigido')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group">
                                                                <div class="input-group input-group-static is-valid mb-4">
                                                                    <label for="cargo">Cargo :<span class="text-danger">(*)</span></label>
                                                                    <input type="text" name="cargo_dirigido" id="cargo_dirigido" class="form-control"  value="{{old('cargo_dirigido')}}">
                                                                </div>                                                       
                                                        </div>
                                                        @error('cargo_dirigido')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group">
                                                                <div class="input-group input-group-static is-valid mb-4">
                                                                    <label for="unidad">Unidad :<span class="text-danger">(*)</span></label>
                                                                    <input type="text" name="unidad_dirigido" id="unidad_dirigido" class="form-control"  value="{{old('unidad_dirigido')}}">
                                                                </div>                                                       
                                                        </div>
                                                        @error('unidad_dirigido')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group">
                                                                <div class="input-group input-group-static is-valid mb-4">
                                                                    <label for="referencia">Referencia :<span class="text-danger">(*)</span></label>
                                                                    <input type="text" name="referencia" id="referencia" class="form-control"  value="{{old('referencia')}}">
                                                                </div>                                                       
                                                        </div>
                                                        @error('referencia')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group">
                                                                <div class="input-group input-group-static is-valid mb-4">
                                                                    <label for="tipo_informe">Tipo Informe :<span class="text-danger">(*)</span></label>
                                                                    <select class="form-control" name="tipo_informe" id="tipo_informe">
                                                                        <option value="{{old('tipo_informe')}}">* Seleccione una opción *</option>
                                                                        @foreach($tipoinforme as $tipoinformes)
                                                                        <option value="{{$tipoinformes->nombre}}">{{$tipoinformes->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>                                                       
                                                        </div>
                                                        @error('tipo_informe')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-4 mt-3">
                                                        <div class="form-group">
                                                                <div class="input-group input-group-static is-valid mb-4">
                                                                    <label for="fecha">Fecha :<span class="text-danger">(*)</span></label>
                                                                    <input type="date" name="fecha" id="fecha" class="form-control"  value="{{old('fecha')}}" min="new Date();">
                                                                </div>                                                       
                                                        </div>
                                                        @error('fecha')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                    </div>
                                                   

                                                </div>
                                        </div>
                                    </div> 
                                    <div class="card" >
                                            <div class="card-header card-header-info">
                                                    <h1 class="card-title text-center">DATOS DEL INFORME</h1>
                                                    <h4 class="card-title text-center"><span class="text-danger">(*)</span>Campos Obligatorios</h4>
                                            </div>
                                            <div class="card-body">
                                                <!-- Formulario de funcionario-->
                                                <div class="row">                        
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                                <div class="input-group input-group-static is-valid mb-4">
                                                                    <label for="dato_informe">Contenido: <span class="text-danger">(*)</span></label>
                                                                    <textarea class="ckeditor" name="dato_informe" id="dato_informe" rows="10" cols="80">{{old('dato_informe')}}</textarea>
                                                                    <!--<input type="text" name="dato_informe" id="dato_informe" class="form-control"  value="{{old('dato_informe')}}">-->
                                                                </div>                                                       
                                                        </div> 
                                                    </div>
                                                    @error('dato_informe')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                </div>
                                                <!-- Fin Formulario de funcionario-->
                                                <!--Imagen a subir-->
                                                <style>
                                                        .file-upload {
                                                        background-color: #ffffff;
                                                        width: 600px;
                                                        margin: 0 auto;
                                                        padding: 20px;
                                                        }

                                                        .file-upload-btn {
                                                        width: 100%;
                                                        margin: 0;
                                                        color: #fff;
                                                        background: #1FB264;
                                                        border: none;
                                                        padding: 10px;
                                                        border-radius: 4px;
                                                        border-bottom: 4px solid #15824B;
                                                        transition: all .2s ease;
                                                        outline: none;
                                                        text-transform: uppercase;
                                                        font-weight: 700;
                                                        }

                                                        .file-upload-btn:hover {
                                                        background: #1AA059;
                                                        color: #ffffff;
                                                        transition: all .2s ease;
                                                        cursor: pointer;
                                                        }

                                                        .file-upload-btn:active {
                                                        border: 0;
                                                        transition: all .2s ease;
                                                        }

                                                        .file-upload-content {
                                                        display: none;
                                                        text-align: center;
                                                        }

                                                        .file-upload-input {
                                                        position: absolute;
                                                        margin: 0;
                                                        padding: 0;
                                                        width: 100%;
                                                        height: 100%;
                                                        outline: none;
                                                        opacity: 0;
                                                        cursor: pointer;
                                                        }

                                                        .image-upload-wrap {
                                                        margin-top: 20px;
                                                        border: 4px dashed #1FB264;
                                                        position: relative;
                                                        }

                                                        .image-dropping,
                                                        .image-upload-wrap:hover {
                                                        background-color: #1FB264;
                                                        border: 4px dashed #ffffff;
                                                        }

                                                        .image-title-wrap {
                                                        padding: 0 15px 15px 15px;
                                                        color: #222;
                                                        }

                                                        .drag-text {
                                                        text-align: center;
                                                        }

                                                        .drag-text h3 {
                                                        font-weight: 100;
                                                        text-transform: uppercase;
                                                        color: #15824B;
                                                        padding: 60px 0;
                                                        }

                                                        .file-upload-image {
                                                        max-height: 200px;
                                                        max-width: 200px;
                                                        margin: auto;
                                                        padding: 20px;
                                                        }

                                                        .remove-image {
                                                        width: 200px;
                                                        margin: 0;
                                                        color: #fff;
                                                        background: #cd4535;
                                                        border: none;
                                                        padding: 10px;
                                                        border-radius: 4px;
                                                        border-bottom: 4px solid #b02818;
                                                        transition: all .2s ease;
                                                        outline: none;
                                                        text-transform: uppercase;
                                                        font-weight: 700;
                                                        }

                                                        .remove-image:hover {
                                                        background: #c13b2a;
                                                        color: #ffffff;
                                                        transition: all .2s ease;
                                                        cursor: pointer;
                                                        }

                                                        .remove-image:active {
                                                        border: 0;
                                                        transition: all .2s ease;
                                                        }
                                                </style>
                                                <script>
                                                    function readURL(input) {
                                                    if (input.files && input.files[0]) {

                                                        var reader = new FileReader();

                                                        reader.onload = function(e) {
                                                        $('.image-upload-wrap').hide();

                                                        $('.file-upload-image').attr('src', e.target.result);
                                                        $('.file-upload-content').show();

                                                        $('.image-title').html(input.files[0].name);
                                                        };

                                                        reader.readAsDataURL(input.files[0]);

                                                    } else {
                                                        removeUpload();
                                                    }
                                                    }

                                                    function removeUpload() {
                                                    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                                                    $('.file-upload-content').hide();
                                                    $('.image-upload-wrap').show();
                                                    }
                                                    $('.image-upload-wrap').bind('dragover', function () {
                                                            $('.image-upload-wrap').addClass('image-dropping');
                                                        });
                                                        $('.image-upload-wrap').bind('dragleave', function () {
                                                            $('.image-upload-wrap').removeClass('image-dropping');
                                                    });

                                                </script>
                                                <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
                                                    <div class="file-upload" style="display:none">
                                                        <!--<button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Agregar Imagen</button>-->
                                                        <div class="image-upload-wrap">
                                                            <input class="file-upload-input" type='file' name="archivo_del_informe" id="archivo_del_informe" value="{{old('archivo_del_informe')}}" onchange="readURL(this);" accept=".doc, .docx,.pdf, .txt, .xlsx,.pptx" />
                                                            <div class="drag-text">
                                                            <h3>Arrastre y suelte un archivo o haga click Aqui</h3>
                                                            </div>
                                                        </div>
                                                        <div class="file-upload-content">
                                                            <img src="/img/file.png" alt="Tu Archivo" width="20%"/>
                                                            <div class="image-title-wrap">
                                                            <button type="button" onclick="removeUpload()" class="remove-image">Remover Archivo:<span class="image-title">Archivo Subido</span></button>
                                                            </div>
                                                        </div>
                                                        @error('archivo_del_informe')
                                                            <p class='text-danger inputerror'>{{ $message }} </p>
                                                        @enderror 
                                                    </div>
                                                <!-- fin imagen subir-->
                                                <!--boton para guardar funcionario-->
                                                <div class="row" >                                                                         
                                                    <div class="col-12 col-sm-6 col-md-4 mt-3">
                                                        <input type="submit" value="Guardar Informe" class="btn btn-success">
                                                    </div>

                                                    <div class="col-12 col-sm-6 col-md-4 mt-3">
                                                        <a type="button" class="btn btn-danger" href="{{ route('billing') }}">Volver Atras</a>
                                                    </div>
                                                </div>
                                                <!--Fin boton guardar funcionario-->
                                                </div>
                                                </div>                    
                                                </div>
                                    </div>

                                </form> 
                                <!-- fin cardformulario de oficinas-->        
                            </div>
                    </div>
                </div>
                <x-footers.auth></x-footers.auth>
            </div>
            
            
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<!--script data table-->
<script>
    /*$(document).ready(function () {
        $('#example').DataTable();
    });*/
    $('#example').DataTable( {
        responsive: true
    } );
</script>
<!--fin data table-->
<!--Script llenar datos de los funcionarios-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $("#nombre_dirigidos").on('change', function () {
        
        /**Recuperacion del cargo */
        var val=$('#nombre_dirigidos').val();
        var cargo = $('#nombre_dirigido').find('option[value="'+val+'"]').data('cargo');
        if(cargo == undefined){
            console.log("se cumple cargo");
            document.getElementById("cargo_dirigido").value= ""    
        }else{
        document.getElementById("cargo_dirigido").value = cargo
        }
        /**Fin recuperacion del cargo */
        /**Recuperacion de la unidad */
        var unidad = $('#nombre_dirigido').find('option[value="'+val+'"]').data('unidad');
        if(unidad == undefined){
            console.log("se cumple unidad");
        document.getElementById("unidad_dirigido").value = ""
        }else{
        document.getElementById("unidad_dirigido").value = unidad
        }
        /**Fin Recuperacion de la unidad */

    });
</script>
<!--Fin de Script del llenado de los funcionarios-->

        </main>
        <x-plugins></x-plugins>
</x-layout>
